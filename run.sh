#!/bin/sh

# -----------------------------------------------------------------------------------------
# Wrapper for dem-interp
# -----------------------------------------------------------------------------------------

# Program args
IN="/home/peet/workspace/dem-interp/inp/WB_DTM.asc"
XS="/home/peet/workspace/dem-interp/inp/WB_XS.csv"
OUT="WB_DTM_Interp"
XLL=344940
YLL=114355
ANGLE=35.0
LENGTH=885
WIDTH=240

# Timing results format
TIME_FMT="\nreal\t%E\nuser\t%U\nsys\t%S\ncpu\t%P\nmem\t%M KB"

if [ "$1" = "debug" ]; then
	# Debugging version
	GDB="gdb -ex run -ex quit --args"
	make clean && make debug && \
	time -f "$TIME_FMT" \
	$GDB bin/dem-interp -i "$IN" -s "$XS" -o "$OUT" -x $XLL -y $YLL -a $ANGLE -l $LENGTH -w $WIDTH
else
	# Release version
	make && \
	time -f "$TIME_FMT" \
	bin/dem-interp -i "$IN" -s "$XS" -o "$OUT" -x $XLL -y $YLL -a $ANGLE -l $LENGTH -w $WIDTH
fi
