# Main directories
BINDIR = bin
DOCDIR = doc
INPDIR = inp
OBJDIR = obj
OUTDIR = out
SRCDIR = src

# Directories to create if not already existant
MKDIR := $(OBJDIR) $(BINDIR) $(INPDIR) $(OUTDIR)

# Binary file
BIN := $(BINDIR)/dem-interp

# CPP object files
CPP_SRC := $(wildcard $(SRCDIR)/*.cpp)
CPP_OBJ := $(patsubst %.cpp, %.o, $(subst $(SRCDIR), $(OBJDIR), $(CPP_SRC)))

# Dependency file
DEP = .depend

# Doxygen config and output files
DOXY_SRC = .doxyfile
DOXY_HTML := $(DOCDIR)/html/index.html

# Compiler options
CXXFLAGS = -c -std=c++11 -O3 -flto -D_USE_MATH_DEFINES -Wall

# Linker options
LDFLAGS = -O3 -flto
LDLIBS = -lshp -lalglib


# NON-FILE TARGETS
.PHONY: all debug doc clean

# DEFAULT TARGET
all: $(MKDIR) $(BIN)

# DEBUG TARGET
debug: CXXFLAGS := -g -pg -DEBUG $(filter-out -O% -flto, $(CXXFLAGS))
debug: LDFLAGS := -g -pg $(filter-out -O% -flto, $(LDFLAGS))
debug: all

# DOXYGEN TARGET
doc: $(DOXY_HTML)

# CREATE DOXYGEN DOCS
$(DOXY_HTML): $(CPP_SRC) $(DOXY_SRC)
	mkdir -p $(DOCDIR)
	doxygen $(DOXY_SRC)

# CREATE DIRS
$(MKDIR):
	mkdir -p $@

# CREATE BINARY
$(BIN): $(CPP_OBJ)
	@$(RM) $(BIN)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@
	@echo "\n**** Compiled on: "`date`" ****"

# DETERMINE DEPENDICES
$(DEP): $(CPP_SRC)
	@$(CXX) -xc++ $(CXXFLAGS) -MM $^ > $(DEP)
	@sed -i 's|.*:|$(OBJDIR)/&|' $(DEP)

-include $(DEP)

# COMPILE C++
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

# CLEAN PROJECT
clean:
	$(RM) $(BIN)
	$(RM) $(DEP)
	$(RM) $(CPP_OBJ)
