// Class header
#include "GlobalArgs.h"

// System includes
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

// Third party includes

// Project includes

// Forward declarations
namespace {	// Anonymous namespace - only visible to current file
	void displayHelp(char** argv, std::string errors);
}

//
// PUBLIC
//

// Constructors
GlobalArgs::GlobalArgs(const int argc, char** argv) :
	mInputFile(""),
	mXSFile(""),
	mOutputFile(""),
	mXllCorner(0.),
	mYllCorner(0.),
	mAngle(0.f),
	mXLength(0.),
	mYLength(0.),
	mCellSize(1.f),
	mNumBuffCells(10),
	mUseQNN(false),
	mRBase(5.f),
	mNumLayers(6)
{
	// Parse command line args
	SetGlobalArgs(argc, argv);
}


// Accessors
const std::string GlobalArgs::GetSummary() const
{
	// Convert floats to strings
	std::stringstream ss;
	std::string rBase;
	std::string numLayers;
	ss << mRBase;
	rBase = ss.str();
	ss.str("");
	ss << mNumLayers;
	numLayers = ss.str();
	ss.str("");

	// Ouput summary of command line options
	ss	<< "\txllcorner\t" << mXllCorner
		<< "\n\tyllcorner\t" << mYllCorner
		<< "\n\tangle\t\t" << mAngle << " deg"
		<< "\n\tlength\t\t" << mXLength << " m"
		<< "\n\twidth\t\t" << mYLength << " m"
		<< "\n\tcellsize\t" << mCellSize << " m2"
		<< "\n\tbuffercells\t" << mNumBuffCells
		<< "\n\talgorithm\t" << (mUseQNN ? "QNN-RBF" : "ML-RBF")
		<< "\n\trbase\t\t" << (mUseQNN ? "N/a" : rBase.c_str())
		<< "\n\tlayers\t\t" << (mUseQNN ? "N/a" : numLayers.c_str());

	return ss.str();
}


//
// PRIVATE
//

// Mutators
void GlobalArgs::SetGlobalArgs(const int argc, char** argv)
{
	// Command line options
	const struct option long_opts[] = {
		{"help",      no_argument,       NULL, 'h'},
		{"input",     required_argument, NULL, 'i'},
		{"xsfile",    required_argument, NULL, 's'},
		{"output",    required_argument, NULL, 'o'},
		{"x-ll",      required_argument, NULL, 'x'},
		{"y-ll",      required_argument, NULL, 'y'},
		{"angle",     required_argument, NULL, 'a'},
		{"x-length",  required_argument, NULL, 'l'},
		{"y-length",  required_argument, NULL, 'w'},
		{"cellsize",  required_argument, NULL, 'c'},
		{"buffcells", required_argument, NULL, 'b'},
		{"use-qnn",   no_argument,       NULL, 'q'},
		{"r-base",    required_argument, NULL, 'r'},
		{"layers",    required_argument, NULL, 'n'},
		{0,0,0,0}
	};

	// Control params
	int opt = 0;
	int long_index = 0;
	const char* p_opt_string = "i:s:o:x:y:a:l:w:c:b:qr:n:h?";

	// Process the arguments with getopt_long(), then populate globalArgs
	opt = getopt_long( argc, argv, p_opt_string, long_opts, &long_index );
	while (opt != -1) {
		switch (opt) {
			case 'i':
				mInputFile = std::string(optarg);
				break;
			case 's':
				mXSFile = std::string(optarg);
				break;
			case 'o':
				mOutputFile = std::string(optarg);
				break;
			case 'x':
				mXllCorner = atof(optarg);
				break;
			case 'y':
				mYllCorner = atof(optarg);
				break;
			case 'l':
				mXLength = atof(optarg);
				break;
			case 'w':
				mYLength = atof(optarg);
				break;
			case 'a':
				mAngle = atof(optarg);
				break;
			case 'c':
				mCellSize = atof(optarg);
				break;
			case 'b':
				mNumBuffCells = atoi(optarg);
				break;
			case 'q':
				mUseQNN = true;
				break;
			case 'r':
				mRBase = atof(optarg);
				break;
			case 'n':
				mNumLayers = atoi(optarg);
				break;
			case 'h':	// Fall-through is intentional
			case '?':
				displayHelp(argv, "");
				break;
		}
		opt = getopt_long(argc, argv, p_opt_string, long_opts, &long_index);
	}

	// Check that all required arguments have been supplied correctly
	CheckArgs(argv);
}


// Accessors
void GlobalArgs::CheckArgs(char** argv) const
{
	bool args_bad = false;
	std::string err = "";

	// Check filenames
	if (mInputFile == "") {
		args_bad = true;
		err += "ERROR: Please enter an input filename (-i)\n";
	}
	if (mOutputFile == "") {
		args_bad = true;
		err += "ERROR: Please enter an output filename (-o)\n";
	}

	// Check new DEM parameters
	if (mXllCorner == 0. && mYllCorner == 0.) {
		args_bad = true;
		err += "ERROR: Please enter valid lower-left co-ordinates (-x, -y)\n";
	}
	if (mXLength <= 0.) {
		args_bad = true;
		err += "ERROR: Please enter a valid x-direction length (-l > 0.)\n";
	}
	if (mYLength <= 0.) {
		args_bad = true;
		err += "ERROR: Please enter a valid y-direction length (-w > 0.)\n";
	}
	if (mCellSize <= 0.f) {
		args_bad = true;
		err += "ERROR: Please enter a valid cellsize (-c > 0.f)\n";
	}

	// Check interpolation coefficients
	if (mNumBuffCells < 0) {
		args_bad = true;
		err += "ERROR: Please enter a valid number of buffer cells (-b >= 0)\n";
	}
	if (mRBase <= 0.f) {
		args_bad = true;
		err += "ERROR: Please enter a valid RBase for the ML-RBF model (-r > 0.f)\n";
	}
	if (mNumLayers <= 0) {
		args_bad = true;
		err += "ERROR: Please enter a valid number of ML-RBF layers (-n > 0)\n";
	}

	if (args_bad) {
		displayHelp(argv, err);
	}
}


//
// FREE FUNCTIONS
//
namespace {	// Anonymous namespace - only visible to current file
	void displayHelp(char** argv, std::string errors)
	{
		std::cout	<< "\nUSAGE: " << argv[0] << " [OPTIONS]\n\n"
					<< "OPTIONS:\n"
					<< "  -i, --input=[\"\"]\n\tXYZ datafile in ESRI grid or CSV format\n"
					<< "  -s, --xsfile=[\"\"]\n\tList of XS points in CSV format\n"
					<< "  -o, --output=[\"\"]\n\tOutput file for new DEM (without extension)\n"
					<< "  -x, --x-ll=[0.]\n\tX co-ord of lower-left corner of new DEM in OS\n"
					<< "  -y, --y-ll=[0.]\n\tY co-ord of lower-left corner of new DEM in OS\n"
					<< "  -a, --angle=[0.f]\n\tRotation angle of new DEM in degrees\n"
					<< "  -l, --x-length=[0.]\n\tLength of new DEM in m\n"
					<< "  -w, --y-length=[0.]\n\tWidth of new DEM in m\n"
					<< "  -c, --cellsize=[1.f]\n\tGrid size of new DEM in m\n"
					<< "  -b, --buffcells=[10]\n\tNumber of buffer cells to use during interpolation\n"
					<< "  -q, --use-qnn\n\tUse QNN-RBF instead of ML-RBF (faster, but less accurate)\n"
					<< "  -r, --r-base=[5.f]\n\tInitial RBase in ML-RBF algorithm\n"
					<< "  -n, --layers=[6]\n\tNumber of layers in ML-RBF algorithm\n"
					<< "  -h, --help\n\tDisplay this message\n\n"
					<< "NOTES:\n"
					<< "\tDefault values shown in square brackets\n"
					<< "\tCSV data files should have the following header: \"X\",\"Y\",\"Z\"\n"
					<< "\tXS data files should have the following header: \"N\",\"X\",\"Y\",\"Z\"\n"
					<< "\t\twhere N is the cross-section number\n\n"
					<< errors << "\n";
		exit(EXIT_FAILURE);
	}
}
