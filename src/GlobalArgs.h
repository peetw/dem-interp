#pragma once

// System includes
#include <string>

// Third party includes

// Project includes

// Forward declarations

// Class declaration
class GlobalArgs {
 public:
	// Constructors
	GlobalArgs() = delete;	// Disable default constructor
	GlobalArgs(const int argc, char** argv);

	// Accessors
	const std::string InputFilename() const {return mInputFile;}
	const std::string XSFilename() const {return mXSFile;}
	const std::string OutputFilename() const {return mOutputFile;}
	const double XllCorner() const {return mXllCorner;}
	const double YllCorner() const {return mYllCorner;}
	const float Angle() const {return mAngle;}
	const double XLength() const {return mXLength;}
	const double YLength() const {return mYLength;}
	const float CellSize() const {return mCellSize;}
	const int NumBuffCells() const {return mNumBuffCells;}
	const bool UseQNN() const {return mUseQNN;}
	const float RBase() const {return mRBase;}
	const int NumLayers() const {return mNumLayers;}

	const std::string GetSummary() const;
	const std::string GetInputFileExt() const;
 private:
	// Member variables
	std::string mInputFile;
	std::string mXSFile;
	std::string mOutputFile;
	double mXllCorner;
	double mYllCorner;
	float mAngle;
	double mXLength;
	double mYLength;
	float mCellSize;
	int mNumBuffCells;
	bool mUseQNN;
	float mRBase;
	int mNumLayers;

	// Mutators
	void SetGlobalArgs(const int argc, char** argv);

	// Accessors
	void CheckArgs(char**) const;
};

// Inline functions
inline const std::string GlobalArgs::GetInputFileExt() const
{
	if (mInputFile.find_last_of(".") != std::string::npos) {
		return mInputFile.substr(mInputFile.find_last_of(".") + 1);
	}

	return "";
}
