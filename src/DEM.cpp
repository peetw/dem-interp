// Class header
#include "DEM.h"

// System includes
#include <algorithm>
#include <cfloat>
#include <cmath>
#include <climits>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

// Third party includes
#include <alglib/interpolation.h>
#include <shapefil.h>

// Project includes
#include "GlobalArgs.h"
#include "Point3d.h"

// Forward declarations
namespace {	// Anonymous namespace - only visible to current file
	const std::string basenameLinux(std::string filename);
	const std::vector<Point3d> expandVertices(const std::vector<Point3d>& rVertices, const double bufferDist);
}

//
// PUBLIC
//

// Constructors
DEM::DEM(const GlobalArgs& rGlobalArgs, const DemType demType) :
	mNumCols(0),
	mNumRows(0),
	mXllCorner(0.),
	mYllCorner(0.),
	mXLength(0.),
	mYLength(0.),
	mCellSize(0.f),
	mNoData(0.f),
	mAngle(0.f),
	mPoints(),
	mVertices(),
	mModel()
{
	// Read in DEM from file
	if (demType == TYPE_DEM) {
		const std::string filename = rGlobalArgs.InputFilename();

		// Check file type
		const std::string ext = rGlobalArgs.GetInputFileExt();
		if (ext == "asc") {
			std::cout << "\nReading DEM from:\n\t\"" << filename << "\"\n";
			ReadEsriGrid(filename);
		} else if (ext == "csv") {
			std::cout << "\nReading DEM from:\n\t\"" << filename << "\"\n";
			ReadCsv(filename);
		} else {
			std::stringstream ss;
			ss	<< "Filetype not supported. Please specify either:\n"
				<< "\tESRI grid *.asc file\n"
				<< "\tXYZ *.csv file";
			throw std::runtime_error(ss.str());
		}
		// Check DEM validity
		if (mPoints.empty()) {
			std::string err = "No valid points found in \"" + filename + "\"";
			throw std::runtime_error(err.c_str());
		} else {
			std::cout << "\nDEM statistics:\n" << GetSummary() << "\n";
		}
	}

	// Read in cross-section points
	if (demType == TYPE_XS) {
		// Get input filename
		const std::string filename = rGlobalArgs.XSFilename();
		std::cout << "\nReading XS from:\n\t\"" << filename << "\"\n";
		ReadCsvXS(filename);
		InterpXS(rGlobalArgs);
		std::cout << "\nInterpolated XS statistics:\n" << GetSummary() << "\n";
	}
}


// Mutators
void DEM::MergeXS(const DEM& xs)
{
	// Remove any old points within or near cross-sections polygon
	const double bufferDist = 1 * mCellSize;
	std::vector<Point3d> vertices = expandVertices(xs.mVertices, bufferDist);
	auto it = mPoints.begin();
	while (it != mPoints.end()) {
		if (it->IsPointInPolygon(vertices.size()-1, vertices.data())) {
			it = mPoints.erase(it);
		} else {
			++it;
		}
	}

	// Add XS points to DEM
	for (auto it = xs.mPoints.begin(); it != xs.mPoints.end(); ++it) {
		mPoints.push_back(*it);
	}
	std::cout << "\nDEM statistics (overlapping XS points replaced):\n" << GetSummary() << "\n";
}

void DEM::SortAscByXyz()
{
	std::cout << "\nSorting DEM by XYZ (ascending)\n";
	std::sort(mPoints.begin(), mPoints.end(), Point3d::CompareAsc);
}

void DEM::SortDescByXyz()
{
	std::cout << "\nSorting DEM by XYZ (descending)\n";
	std::sort(mPoints.begin(), mPoints.end(), Point3d::CompareDesc);
}


// Accessors
const std::string DEM::GetSummary() const
{
	// Ouput DEM stats
	std::stringstream ss;

	ss	<< "\ttotalcells\t" << mPoints.size()
		<< "\n\txllcorner\t" << mXllCorner
		<< "\n\tyllcorner\t" << mYllCorner
		<< "\n\tangle\t\t" << mAngle << " deg"
		<< "\n\tcellsize\t" << mCellSize << " m2";

	return ss.str();
}

const DEM DEM::Interp(const GlobalArgs& rGlobalArgs) const
{
	std::cout << "\nInterpolating new DEM with:\n" << rGlobalArgs.GetSummary() << "\n";

	// Initialise new DEM
	DEM new_dem;

	// Store new params
	new_dem.mXllCorner = rGlobalArgs.XllCorner();
	new_dem.mYllCorner = rGlobalArgs.YllCorner();
	new_dem.mXLength = rGlobalArgs.XLength();
	new_dem.mYLength = rGlobalArgs.YLength();
	new_dem.mCellSize = rGlobalArgs.CellSize();
	new_dem.mNumCols = floor(new_dem.mXLength / new_dem.mCellSize) + 1;
	new_dem.mNumRows = floor(new_dem.mYLength / new_dem.mCellSize) + 1;
	new_dem.mAngle = rGlobalArgs.Angle();
	new_dem.mNoData = mNoData;

	// Find vertices of new DEM
	new_dem.SetVerticesRect();

	// Get old DEM points within and around new DEM
	std::vector<Point3d> search_points = GetSearchPoints(new_dem, rGlobalArgs.NumBuffCells());

	// Check that new DEM is within old DEM
	if (search_points.empty()) {
		std::string err = "No points within the specified interpolation region were found in:\n\t\"" + rGlobalArgs.InputFilename() + "\"";
		throw std::runtime_error(err.c_str());
	}

	// Create radial basis function model from old DEM
	new_dem.mModel = BuildRbfModel(search_points, rGlobalArgs.UseQNN(), rGlobalArgs.RBase(), rGlobalArgs.NumLayers());

	// Use model to determine elevations in new DEM
	new_dem.mPoints = new_dem.CalcRbfModel();

	// Display new DEM statistics
	std::cout << "\nNew DEM statistics:\n" << new_dem.GetSummary() << "\n";

	// Return new DEM
	return new_dem;
}

void DEM::OutputCsv(std::string filename) const
{
	// Open output file
	filename = "out/" + basenameLinux(filename) + ".csv";
	std::ofstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open output file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	} else {
		std::cout << "\nOutputting points to:\n\t\"" << filename << "\"\n";
	}

	// Write header
	file << "\"X\",\"Y\",\"Z\"\n";
	file.setf(std::ios::fixed, std::ios::floatfield);
	file.precision(2);

	// Output elevations
	for (auto it = mPoints.begin(); it != mPoints.end(); ++it) {
		file	<< it->x << ","
				<< it->y << ","
				<< it->z << "\n";
	}
}

void DEM::OutputDivast(const std::string baseFilename) const
{
	// Create temporary DEM and convert degrees to radians
	DEM tmp = (*this);
	const float ang_rad = mAngle * M_PI/180.f;

	// Open X direction output file
	std::string filename = "out/" + basenameLinux(baseFilename) + "_X.dat";
	std::ofstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open output file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	} else {
		std::cout << "\nOutputting points to:\n\t\"" << filename << "\"\n";
	}

	// Get X direction side centered positions
	tmp.mXllCorner -= .5f * mCellSize * cos(ang_rad);
	tmp.mYllCorner -= .5f * mCellSize * sin(ang_rad);
	tmp.mNumCols = mNumCols + 1;
	tmp.mNumRows = mNumRows;

	// Get X direction elevations
	tmp.mPoints = tmp.CalcRbfModel();

	// Output X direction elevations (depth below datum)
	file.setf(std::ios::fixed,std::ios::floatfield);
	file.precision(2);
	for (std::size_t i = 0; i < tmp.mNumRows; ++i) {
		for (std::size_t j = 0; j < tmp.mNumCols; ++j) {
			const std::size_t idx = i*tmp.mNumCols + j;
			file << -1.*tmp.mPoints[idx].z << " ";
		}
		file << "\n";
	}
	file.close();

	// Open Y direction output file
	filename = "out/" + basenameLinux(baseFilename) + "_Y.dat";
	file.open(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open output file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	} else {
		std::cout << "\nOutputting points to:\n\t\"" << filename << "\"\n";
	}

	// Get Y direction side centered positions
	tmp.mXllCorner -= .5f * mCellSize * sin(ang_rad);
	tmp.mYllCorner -= .5f * mCellSize * cos(ang_rad);
	tmp.mNumCols = mNumCols;
	tmp.mNumRows = mNumRows + 1;

	// Get Y direction elevations
	tmp.mPoints = tmp.CalcRbfModel();

	// Output Y direction elevations (depth below datum)
	file.setf(std::ios::fixed,std::ios::floatfield);
	file.precision(2);
	for (std::size_t i = 0; i < tmp.mNumRows; ++i) {
		for (std::size_t j = 0; j < tmp.mNumCols; ++j) {
			const std::size_t idx = i*tmp.mNumCols + j;
			file << -1.*tmp.mPoints[idx].z << " ";
		}
		file << "\n";
	}
}

void DEM::OutputEsriGrid(std::string filename) const
{
	// Open output file
	filename = "out/" + basenameLinux(filename) + ".asc";
	std::ofstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open output file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	} else {
		std::cout << "\nOutputting points to:\n\t\"" << filename << "\"\n";
	}

	// Write header
	file << "ncols        " << mNumCols << "\n";
	file << "nrows        " << mNumRows << "\n";
	file << "xllcorner    " << mXllCorner << "\n";
	file << "yllcorner    " << mYllCorner << "\n";
	file << "cellsize     " << mCellSize << "\n";
	file << "NODATA_value " << -9999 << "\n";

	// Output elevations in mm
	for (std::size_t i = 0; i < mNumRows; ++i) {
		for (std::size_t j = 0; j < mNumCols; ++j) {
			std::size_t idx = (mNumRows-1 - i)*mNumCols + j;
			file << std::left << std::setw(6) << std::fixed << std::setprecision(0) << mPoints[idx].z * 1000;
		}
		file << "\n";
	}
}

void DEM::OutputEsriShape(std::string filename) const
{
	// Open output shapefile
	filename = "out/" + basenameLinux(filename) + "_Extent";
	SHPHandle shp_handle = SHPCreate(filename.c_str(), SHPT_POLYGON);

	std::cout << "\nOutputting shapefile to:\n\t\"" << filename << ".shp\"\n";

	// Store DEM vertices in plain arrays
	const std::size_t num_vertices = mVertices.size();
	double x_vertices[num_vertices];
	double y_vertices[num_vertices];
	for (std::size_t i = 0; i < num_vertices; ++i) {
		x_vertices[i] = mVertices[i].x;
		y_vertices[i] = mVertices[i].y;
	}

	// Create shapefile object from DEM vertices
    SHPObject* p_shp_object = SHPCreateSimpleObject(SHPT_POLYGON, num_vertices, x_vertices, y_vertices, NULL);

	// Write shapefile object to file and clean up
	SHPWriteObject(shp_handle, -1, p_shp_object);
	SHPDestroyObject(p_shp_object);
	SHPClose(shp_handle);

	// Open output xbase file
	filename = filename + ".dbf";
	DBFHandle dbf_handle = DBFCreate(filename.c_str());

	// Add fields to xbase file
	const unsigned int field_width = 10;
	const unsigned int field_decimals = 0;
	DBFAddField(dbf_handle, "id", FTInteger, field_width, field_decimals);

	// Write NULL and clean up
	DBFWriteNULLAttribute(dbf_handle, 0, 0);
	DBFClose(dbf_handle);
}

void DEM::OutputTvdDomain(std::string filename) const
{
	// Open output file
	filename = "out/" + basenameLinux(filename) + "_domain.dat";
	std::ofstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open output file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	} else {
		std::cout << "\nOutputting domain specification to:\n\t\"" << filename << "\"\n";
	}

	// Write header
	file << "HEADER (DOMAIN DESCRIP)\n";

	// Specify domain
	for (std::size_t i = 0; i < mNumRows; ++i) {
		for (std::size_t j = 0; j < mNumCols; ++j) {
			if (i == 0 || i == mNumRows-1 || j == 0 || j == mNumCols-1) {
				file << "0";
			} else {
				file << "1";
			}
		}
		file << "\n";
	}
}

void DEM::OutputTvdInitial(std::string filename, const DEM& river) const
{
	// Open output file
	filename = "out/" + basenameLinux(filename) + "_initial.dat";
	std::ofstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open output file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	} else {
		std::cout << "\nOutputting initial values to:\n\t\"" << filename << "\"\n";
	}

	// Write header
	file << "HEADER (INITIAL VALUES): I, J, H, E, QX, QY, VCHZ, QM\n";

	// Specify initial values
	for (std::size_t i = 0; i < mNumRows; ++i) {
		for (std::size_t j = 0; j < mNumCols; ++j) {
			// Adjust boundary elevations to match inner cells
			std::size_t idx = i*mNumCols + j;
			double bed_elev;
			double water_elev;

			if (i == 0 && j > 0 && j < mNumCols-1) {
				idx += mNumCols;
			} else if (i == mNumRows-1 && j > 0 && j < mNumCols-1) {
				idx -= mNumCols;
			} else if (j == 0 && i > 0 && i < mNumRows-1) {
				idx += 1;
			} else if (j == mNumCols-1 && i > 0 && i < mNumRows-1) {
				idx -= 1;
			}
			bed_elev = -mPoints[idx].z;
			water_elev = mPoints[idx].z;

			// Adjust values for river cells
			idx = i*mNumCols + j;
			float ks = 0.05;
			if (mPoints[idx].IsPointInPolygon(river.mVertices.size()-1, river.mVertices.data())) {
				water_elev += 0.2;
				ks = 0.025;
			}

			// Output values
			file	<< std::left << std::setw(5) << j+1 << std::setw(4) << i+1
					<< std::fixed << std::setprecision(2) << bed_elev << " " << water_elev
					<< " 0" << " 0 "
					<< std::setprecision(3) << ks
					<< " 0\n";
		}
	}
}


//
// PRIVATE
//

// Mutators
void DEM::ReadEsriGrid(const std::string filename)
{
	// Open file
	std::ifstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open input file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	}

	// Read header
	std::string null;
	file	>> null >> mNumCols
			>> null >> mNumRows
			>> null >> mXllCorner
			>> null >> mYllCorner
			>> null >> mCellSize
			>> null >> mNoData;
	getline(file, null);

	// Check format
	if (mNumCols < 2 || mNumRows < 2 || (mXllCorner == 0. && mYllCorner == 0.) || mCellSize <= 0.) {
		std::string err = "Incorrect format for input file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	}

	// Assign derived values
	mXLength = mNumCols * mCellSize;
	mYLength = mNumRows * mCellSize;

	// Find DEM vertices
	SetVerticesRect();

	// Read elevations
	for (std::size_t i = 0; i < mNumRows; ++i) {
		// Get line
		std::string line;
		getline(file, line);
		std::istringstream iss(line);

		// Read in values from line
		for (std::size_t j = 0; j < mNumCols; ++j) {
			// Store point in vector
			Point3d point;
			point.x = mXllCorner + j*mCellSize;
			point.y = mYllCorner + (mNumRows-1-i)*mCellSize;
			iss >> point.z;
			if (point.z != mNoData) {
				mPoints.push_back(point);
			}
		}
	}

	//// Check format
	//if (mPoints.size() != ()) {
		//std:string err = "Incorrect format for input file " + filename;
		//throw std::runtime_error(err.c_str());
	//}
}

void DEM::ReadCsv(const std::string filename)
{
	// Open file
	std::ifstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open input file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	}

	// Read header
	std::string line;
	getline(file, line);

	// Read each line and assign data
	while (getline(file, line)) {
		std::istringstream iss(line);
		std::string cell;
		Point3d point;
		getline(iss, cell, ',');
		point.x = strtod(cell.c_str(), NULL);
		getline(iss, cell, ',');
		point.y = strtod(cell.c_str(), NULL);
		getline(iss, cell, ',');
		point.z = strtod(cell.c_str(), NULL);
		mPoints.push_back(point);
	}
}

void DEM::ReadCsvXS(const std::string filename)
{
	// Open file
	std::ifstream file(filename.c_str());

	// Check file
	if (!file.is_open()) {
		std::string err = "Unable to open input file \"" + filename + "\"";
		throw std::runtime_error(err.c_str());
	}

	// Read header
	std::string line;
	getline(file, line);

	// Initialise variables
	int num_xs = INT_MIN;
	std::vector<Point3d> right_bank;

	// Read each line and assign data
	while (getline(file, line)) {
		std::istringstream iss(line);
		std::string cell;
		// Get XS number
		getline(iss, cell, ',');
		const int num_xs_tmp = atoi(cell.c_str());

		// Get point data
		Point3d point;
		getline(iss, cell, ',');
		point.x = atof(cell.c_str());
		getline(iss, cell, ',');
		point.y = atof(cell.c_str());
		getline(iss, cell, ',');
		point.z = atof(cell.c_str());
		mPoints.push_back(point);

		// Update vertices
		if (num_xs_tmp != num_xs) {
			mVertices.push_back(point);
			if (num_xs != INT_MIN) {
				// Use last point from last cross-section as right bank
				right_bank.push_back(mPoints.at(mPoints.size()-2));
			}
			num_xs = num_xs_tmp;
		}
	}

	// Close polygon with right bank vertices (added in reverse order)
	right_bank.push_back(mPoints.at(mPoints.size()-1));
	for (auto rit = right_bank.rbegin(); rit != right_bank.rend(); ++rit) {
		mVertices.push_back(*rit);
	}
	mVertices.push_back(mVertices.front());
}

void DEM::SetVerticesRect()
{
	// Reset current vertices
	mVertices.clear();

	// Store DEM lower-left corner as first vertex
	Point3d point;
	point.x = mXllCorner;
	point.y = mYllCorner;
	mVertices.push_back(point);

	// Convert angle to radians
	const float ang_rad = mAngle * M_PI/180.f;

	// Determine remaining vertices, moving clockwise
	point.x = mXllCorner + mYLength*cos(M_PI/2.f + ang_rad);
	point.y = mYllCorner + mYLength*sin(M_PI/2.f - ang_rad);
	mVertices.push_back(point);

	point.x = mXllCorner + mXLength*cos(ang_rad) + mYLength*cos(M_PI/2.f + ang_rad);
	point.y = mYllCorner + mXLength*sin(ang_rad) + mYLength*sin(M_PI/2.f - ang_rad);
	mVertices.push_back(point);

	point.x = mXllCorner + mXLength*cos(ang_rad);
	point.y = mYllCorner + mXLength*sin(ang_rad);
	mVertices.push_back(point);

	// Close the polygon
	mVertices.push_back(mVertices.front());
}

void DEM::InterpXS(const GlobalArgs& rGlobalArgs) {
	// Generate model
	mModel = BuildRbfModel(mPoints, false, 5, 6);

	// Find cross-section points extents
	double xMin = DBL_MAX;
	double xMax = -1.*DBL_MAX;
	double yMin = DBL_MAX;
	double yMax = -1.*DBL_MAX;

	for (auto it = mPoints.begin(); it != mPoints.end(); ++it) {
		xMin = (it->x < xMin ? it->x : xMin);
		xMax = (it->x > xMax ? it->x : xMax);
		yMin = (it->y < yMin ? it->y : yMin);
		yMax = (it->y > yMax ? it->y : yMax);
	}

	// Round extents to fit desired resolution
	mCellSize = rGlobalArgs.CellSize() / 2.f;
	xMin = mCellSize * floor(xMin / mCellSize);
	xMax = mCellSize * ceil(xMax / mCellSize);
	yMin = mCellSize * floor(yMin / mCellSize);
	yMax = mCellSize * ceil(yMax / mCellSize);
	mNumCols = (xMax - xMin) / mCellSize + 1;
	mNumRows = (yMax - yMin) / mCellSize + 1;
	mXllCorner = xMin;
	mYllCorner = yMin;

	// Fill cross-section polygon with points at desired resolution
	mPoints.clear();
	for (std::size_t i = 0; i < mNumRows; ++i) {
		for (std::size_t j = 0; j < mNumCols; ++j) {
			Point3d point;
			point.x = mXllCorner + j*mCellSize;
			point.y = mYllCorner + i*mCellSize;
			if (point.IsPointInPolygon(mVertices.size()-1, mVertices.data())) {
				mPoints.push_back(point);
			}
		}
	}

	// Interpolate elevations at desired resolution
	mPoints = CalcRbfModel(mPoints);
}


// Accessors
const std::vector<Point3d> DEM::GetSearchPoints(const DEM& rSearchArea, const int numBuffCells) const
{
	// Get new DEM vertices
	std::vector<Point3d> vertices = rSearchArea.mVertices;

	// Expand vertices to include buffer room
	const double bufferDist = numBuffCells * mCellSize;
	std::vector<Point3d> new_vertices = expandVertices(vertices, bufferDist);

	// Loop through original DEM and select only those points within search area
	std::vector<Point3d> search_points;
	for (auto it = mPoints.begin(); it != mPoints.end(); ++it) {
		if (it->IsPointInPolygon(new_vertices.size()-1, new_vertices.data())) {
			search_points.push_back(*it);
		}
	}

	return search_points;
}

const alglib::real_1d_array DEM::Serialise1D(const std::vector<Point3d>& rSearchPoints, const PointType1D pointType) const
{
	// Initialise string 'array'
	std::stringstream ss;
	ss << "[";

	// Format data in alglib style
	for (auto it = rSearchPoints.begin(); it != rSearchPoints.end(); ++it) {
		if (pointType == X) {
			ss << it->x << ",";
		} else if (pointType == Y) {
			ss << it->y << ",";
		} else if (pointType == Z) {
			ss << it->z << ",";
		}
	}

	// Close string 'array' and return
	std::string data = ss.str();
	data = data.substr(0, data.size()-1) + "]";
	alglib::real_1d_array array(data.c_str());
	return array;
}

const alglib::real_2d_array DEM::Serialise2D(const std::vector<Point3d>& rSearchPoints, const PointType2D pointType) const
{
	// Initialise string 'array'
	std::stringstream ss;
	ss << "[";

	// Format data in alglib style
	for (auto it = rSearchPoints.begin(); it != rSearchPoints.end(); ++it) {
		if (pointType == XX) {
			ss << "[" << it->x << "," << it->x << "],";
		} else if (pointType == XY) {
			ss << "[" << it->x << "," << it->y << "],";
		} else if (pointType == XZ) {
			ss << "[" << it->x << "," << it->z << "],";
		} else if (pointType == YX) {
			ss << "[" << it->y << "," << it->x << "],";
		} else if (pointType == YY) {
			ss << "[" << it->y << "," << it->y << "],";
		} else if (pointType == YZ) {
			ss << "[" << it->y << "," << it->z << "],";
		} else if (pointType == ZX) {
			ss << "[" << it->z << "," << it->x << "],";
		} else if (pointType == ZY) {
			ss << "[" << it->z << "," << it->y << "],";
		} else if (pointType == ZZ) {
			ss << "[" << it->z << "," << it->z << "],";
		} else if (pointType == XYZ) {
			ss << "[" << it->x << "," << it->y << "," << it->z << "],";
		}
	}

	// Close string 'array' and return
	std::string data = ss.str();
	data = data.substr(0, data.size()-1) + "]";
	alglib::real_2d_array array(data.c_str());
	return array;
}

const std::vector<Point3d> DEM::Unserialise(const alglib::real_2d_array& rArray) const
{
	// Initialize
	std::vector<Point3d> points;

	// Note that alglib matrix somehow seems to be transposed
	//std::cout << "Unserial Cols: " << rArray.cols() << "\tRows: " << rArray.rows() << "\n";

	// Convert from alglib format back into Point3d vector
	for (std::size_t i = 0; i < (std::size_t)rArray.rows(); ++i) {
		for (std::size_t j = 0; j < (std::size_t)rArray.cols(); ++j) {
			Point3d point;
			point.x = mXllCorner + i*mCellSize;
			point.y = mYllCorner + j*mCellSize;
			point.z = rArray[i][j];
			points.push_back(point);
		}
	}

	// Return
	return points;
}

const alglib::rbfmodel DEM::BuildRbfModel(const std::vector<Point3d>& rSearchPoints, const bool useQNN,
	const float rBase, const int numLayers) const
{
	// Initialise 2D model with scalar point values
	alglib::rbfmodel model;
	alglib::rbfcreate(2, 1, model);

	// Convert xyz data to alglib format
	alglib::real_2d_array xyz = Serialise2D(rSearchPoints, XYZ);
	alglib::rbfsetpoints(model, xyz);

	// Set model type (QNN or ML)
	if (useQNN) {
		alglib::rbfsetalgoqnn(model);
	} else {
		alglib::rbfsetalgomultilayer(model, rBase, numLayers);
	}

	// Build and return model
	alglib::rbfreport report;
	alglib::rbfbuildmodel(model, report);
	return model;
}

const std::vector<Point3d> DEM::CalcRbfModel() const
{
	std::vector<Point3d> points;

	// Check if points are on cartesian grid
	if (fmodf(mAngle, 90.f) == 0.f) {
		// Get list of x co-ordinates
		alglib::real_1d_array x_vals;
		x_vals.setlength(mNumCols);
		for (std::size_t i = 0; i < mNumCols; ++i) {
			x_vals[i] = mXllCorner + i*mCellSize;
		}

		// Get list of y co-ordinates
		alglib::real_1d_array y_vals;
		y_vals.setlength(mNumRows);
		for (std::size_t i = 0; i < mNumRows; ++i) {
			y_vals[i] = mYllCorner + i*mCellSize;
		}

		// Use model to calculate new values at each grid point
		alglib::real_2d_array interp_values;
		alglib::rbfgridcalc2(mModel, x_vals, mNumCols, y_vals, mNumRows, interp_values);

		// Convert data back from alglib format
		points = Unserialise(interp_values);
	} else {
		// Convert angle to radians
		const float ang_rad = mAngle * M_PI/180.f;

		// Loop over each point and interpolate elevation
		for (std::size_t i = 0; i < mNumRows; ++i) {
			for (std::size_t j = 0; j < mNumCols; ++j) {
				// Calculate current position
				const double xPos = mXllCorner + i*mCellSize*cos(M_PI/2.f+ang_rad) + j*mCellSize*cos(ang_rad);
				const double yPos = mYllCorner + i*mCellSize*sin(M_PI/2.f-ang_rad) + j*mCellSize*sin(ang_rad);

				// Get elevation from RBF model
				const double elev = alglib::rbfcalc2(mModel, xPos, yPos);

				// Store point in vector
				Point3d point(xPos, yPos, elev);
				points.push_back(point);
			}
		}
	}

	return points;
}

const std::vector<Point3d> DEM::CalcRbfModel(const std::vector<Point3d>& rPoints) const
{
	std::vector<Point3d> points;

	for (auto it = rPoints.begin(); it != rPoints.end(); ++it) {
		// Calculate current position
		const double xPos = it->x;
		const double yPos = it->y;

		// Get elevation from RBF model
		const double elev = alglib::rbfcalc2(mModel, xPos, yPos);

		// Store point in vector
		Point3d point(xPos, yPos, elev);
		points.push_back(point);
	}

	return points;
}


//
// FREE FUNCTIONS
//
namespace {	// Anonymous namespace - only visible to current file
	const std::string basenameLinux(std::string filename)
	{
		if (filename.find_last_of("/") != std::string::npos) {
			filename = filename.substr(filename.find_last_of("/") + 1);
		}
		if (filename.find_last_of(".") != std::string::npos) {
			filename = filename.substr(0, filename.find_last_of("."));
		}
		return filename;
	}

	const std::vector<Point3d> expandVertices(const std::vector<Point3d>& rVertices, const double bufferDist)
	{
		// Perpendicular distance to expand by
		const double dist = sqrt(2. * pow(bufferDist, 2.));

		// Initialise expanded vertices container
		std::vector<Point3d> new_vertices(rVertices);

		// Loop through each vertex and add buffer
		for (std::size_t i = 0; i < rVertices.size()-1; ++i) {
			// Get current vertex position and index of previous vertex
			const std::size_t prev = (i == 0 ? rVertices.size()-2 : i-1);
			const double xCurr = rVertices[i].x;
			const double yCurr = rVertices[i].y;

			// Get angles from horizontal to lines joining current vertex with neighbouring vertices
			const float ang_prev = atan2(rVertices[prev].y - yCurr, rVertices[prev].x - xCurr);
			const float ang_next = atan2(rVertices[i+1].y - yCurr, rVertices[i+1].x - xCurr);
			float ang_norm = .5f * (ang_prev + ang_next);
			if (ang_prev < ang_next)
				ang_norm += M_PI;

			// Get new vertex co-ordinates
			new_vertices[i].x = xCurr + dist*cos(ang_norm);
			new_vertices[i].y = yCurr + dist*sin(ang_norm);
		}

		// Close shape and return
		new_vertices.back() = new_vertices.front();
		return new_vertices;
	}
}
