// System includes
#include <exception>
#include <iostream>

// Third party includes

// Project includes
#include "DEM.h"
#include "GlobalArgs.h"

// Main program
int main(int argc, char *argv[]) {
	// Catch any run-time exceptions
	try {
		// Parse command line options
		GlobalArgs global_args(argc, argv);

		// Read in DEM from file
		DEM dem(global_args, DEM::TYPE_DEM);

		// Read in cross-sections and merge into DEM
		DEM xs(global_args, DEM::TYPE_XS);
		xs.OutputEsriShape(global_args.XSFilename());
		xs.OutputCsv("WB_XS_Interp");
		dem.MergeXS(xs);

		 //Interpolate new DEM using specified options
		DEM interp = dem.Interp(global_args);

		// Sort new DEM by X co-ordinates
		//interp.SortAscByXyz();

		// Output interpolated DEM to csv file
		interp.OutputCsv(global_args.OutputFilename());

		// Output interpolated DEM to DIVAST X/Y files
		//interp.OutputDivast(global_args.OutputFilename());

		// Output interpolated DEM to DIVAST-TVD domain and initial value files
		interp.OutputTvdDomain(global_args.OutputFilename());
		interp.OutputTvdInitial(global_args.OutputFilename(), xs);

		// Output interpolated DEM to ESRI grid and shape files
		interp.OutputEsriGrid(global_args.OutputFilename());
		interp.OutputEsriShape(global_args.OutputFilename());

		// Exit successfully
		return 0;
	} catch (std::exception& e) {
		// Output exception and return exit code
		std::cout << "\n\nERROR:\t" << e.what() << "\n\n";
		return 1;
	}
}
