#pragma once

// System includes
#include <cstddef>
#include <string>
#include <vector>

// Third party includes
#include <alglib/interpolation.h>

// Project includes
#include "Point3d.h"

// Forward declarations
class GlobalArgs;

// Class declaration
class DEM {
 public:
	// Enums
	enum DemType {TYPE_DEM, TYPE_XS};

	// Constructors
	DEM() :
		mNumCols(0),
		mNumRows(0),
		mXllCorner(0.),
		mYllCorner(0.),
		mXLength(0.),
		mYLength(0.),
		mCellSize(0.f),
		mNoData(0.f),
		mAngle(0.f),
		mPoints(),
		mVertices() {}
	DEM(const GlobalArgs& rGlobalArgs, const DemType demType);

	// Destructors
	//~DEM();

	// Mutators
	void MergeXS(const DEM& rhs);
	void SortAscByXyz();
	void SortDescByXyz();

	// Accessors
	const std::string GetSummary() const;
	const DEM Interp(const GlobalArgs& rGlobalArgs) const;
	void OutputCsv(std::string filename) const;
	void OutputDivast(const std::string baseFilename) const;
	void OutputEsriGrid(std::string filename) const;
	void OutputEsriShape(std::string filename) const;
	void OutputTvdDomain(std::string filename) const;
	void OutputTvdInitial(std::string filename, const DEM& river) const;
 private:
	// Member variables
	std::size_t mNumCols;
	std::size_t mNumRows;
	double mXllCorner;
	double mYllCorner;
	double mXLength;
	double mYLength;
	float mCellSize;
	float mNoData;
	float mAngle;
	std::vector<Point3d> mPoints;
	std::vector<Point3d> mVertices;
	alglib::rbfmodel mModel;

	// Enums
	enum PointType1D {X, Y, Z};
	enum PointType2D {XX, XY, XZ, YX, YY, YZ, ZX, ZY, ZZ, XYZ};

	// Mutators
	void InterpXS(const GlobalArgs& rGlobalArgs);
	void ReadCsv(const std::string filename);
	void ReadCsvXS(const std::string filename);
	void ReadEsriGrid(const std::string filename);
	void SetVerticesRect();

	// Accessors
	const alglib::rbfmodel BuildRbfModel(const std::vector<Point3d>& rSearchPoints, const bool useQNN, const float rBase, const int numLayers) const;
	const std::vector<Point3d> CalcRbfModel() const;
	const std::vector<Point3d> CalcRbfModel(const std::vector<Point3d>& rPoints) const;
	const std::vector<Point3d> GetSearchPoints(const DEM& rSearchArea, const int numBuffCells) const;
	const alglib::real_1d_array Serialise1D(const std::vector<Point3d>& rSearchPoints, const PointType1D pointType) const;
	const alglib::real_2d_array Serialise2D(const std::vector<Point3d>& rSearchPoints, const PointType2D pointType) const;
	const std::vector<Point3d> Unserialise(const alglib::real_2d_array& rArray) const;
};

// Inline functions
