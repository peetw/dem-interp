#pragma once

// System includes
#include <cmath>
#include <cstddef>

// Third party includes

// Project includes

// Forward declarations

// Struct declaration
struct Point3d {
	// Member variables
	double x;
	double y;
	double z;

	// Constructors
	Point3d() :
		x(0.),
		y(0.),
		z(0.) {}
	Point3d(const double a, const double b, const double c) :
		x(a),
		y(b),
		z(c) {}

	// Destructors

	// Compound assignment operators
	Point3d& operator+=(const Point3d&);
	Point3d& operator-=(const Point3d&);
	Point3d& operator*=(const Point3d&);

	// Binary operators
	const Point3d operator+(const Point3d&) const;
	const Point3d operator-(const Point3d&) const;
	const Point3d operator*(const Point3d&) const;

	// Mutators

	// Accessors
	static bool CompareAsc(const Point3d&, const Point3d&);
	static bool CompareDesc(const Point3d&, const Point3d&);
	const double Magnitude() const;
	const double DotProduct(const Point3d&) const;
	const Point3d CrossProduct(const Point3d&) const;
	const Point3d Normalize() const;
	const int IsLeftOfLine(const Point3d&, const Point3d&) const;
	const int IsPointInPolygon(const std::size_t numVertices, const Point3d* const pVertices) const;
};

// Inline functions
inline const double Point3d::Magnitude() const
{
	return sqrt(x*x + y*y + z*z);
}

inline const double Point3d::DotProduct(const Point3d& rhs) const
{
	return x*rhs.x + y*rhs.y + z*rhs.z;
}

inline const int Point3d::IsLeftOfLine(const Point3d& rP0, const Point3d& rP1) const
{
	return ((rP1.x - rP0.x) * (y - rP0.y) - (x - rP0.x) * (rP1.y - rP0.y));
}
