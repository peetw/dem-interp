// Class header
#include "Point3d.h"

// System includes
#include <cstddef>

// Third party includes

// Project includes

// Assignment operators
Point3d& Point3d::operator+=(const Point3d& rhs) {
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	return *this;
}

Point3d& Point3d::operator-=(const Point3d& rhs) {
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	return *this;
}

Point3d& Point3d::operator*=(const Point3d& rhs) {
	*this = CrossProduct(rhs);
	return *this;
}


// Binary operators
const Point3d Point3d::operator+(const Point3d& rhs) const
{
	Point3d tmp = *this;
	tmp += rhs;
	return tmp;
}

const Point3d Point3d::operator-(const Point3d& rhs) const
{
	Point3d tmp = *this;
	tmp -= rhs;
	return tmp;
}

const Point3d Point3d::operator*(const Point3d& rhs) const
{
	Point3d tmp = *this;
	tmp *= rhs;
	return tmp;
}


// Accessors
bool Point3d::CompareAsc(const Point3d& lhs, const Point3d& rhs)
{
	if (lhs.x != rhs.x)
		return lhs.x < rhs.x;

	if (lhs.y != rhs.y)
		return lhs.y < rhs.y;

	return lhs.z < rhs.z;
}

bool Point3d::CompareDesc(const Point3d& lhs, const Point3d& rhs)
{
	if (lhs.x != rhs.x)
		return lhs.x > rhs.x;

	if (lhs.y != rhs.y)
		return lhs.y > rhs.y;

	return lhs.z > rhs.z;
}

const Point3d Point3d::CrossProduct(const Point3d& rhs) const
{
	Point3d tmp;
	tmp.x = y*rhs.z - z*rhs.y;
	tmp.y = z*rhs.x - x*rhs.z;
	tmp.z = x*rhs.y - y*rhs.x;
	return tmp;
}

const Point3d Point3d::Normalize() const
{
	const double mag = Magnitude();
	Point3d tmp;
	tmp.x = x / mag;
	tmp.y = y / mag;
	tmp.z = z / mag;
	return tmp;
}

const int Point3d::IsPointInPolygon(const std::size_t numVertices, const Point3d* const pVertices) const
{
	// See: http://geomalgorithms.com/a03-_inclusion.html

	// Initialise winding number
	int wn = 0;

	// Loop through all edges of the polygon
	for (std::size_t i = 0; i < numVertices; ++i) {
		if (pVertices[i].y <= y) {
			if (pVertices[i+1].y > y)
				if (IsLeftOfLine(pVertices[i], pVertices[i+1]) > 0)
					++wn;
		} else {
			if (pVertices[i+1].y <= y)
				if (IsLeftOfLine(pVertices[i], pVertices[i+1]) < 0)
					--wn;
		}
	}
	// Final winding number: 0 if outside, != 0 if inside
	return wn;
}
